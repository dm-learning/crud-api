package com.dipankar.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dipankar.entity.Employee;
import com.dipankar.repository.EmployeeRepository;

@Service
public class EmployeeService {
	
	@Autowired
	private EmployeeRepository repository;
	
	public List<Employee> getAllEmployee(){
		
		List<Employee> employeeList = new ArrayList();
		
		repository.findAll().forEach( emp -> {
			employeeList.add(emp);
		});
		
		return employeeList;
	}
	
	public Employee saveEmployee(Employee employee) {
		return repository.save(employee);
	}
	
	public void deleteEmployee(Integer id) {
		repository.deleteById(id);
	}

}
