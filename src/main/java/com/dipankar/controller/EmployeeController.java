package com.dipankar.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dipankar.entity.Employee;
import com.dipankar.service.EmployeeService;

@RestController
@RequestMapping(value = "/employee")
@CrossOrigin(origins = "*")
public class EmployeeController {
	
	@Autowired
	private EmployeeService service;
	
	@GetMapping ( value = "/all")
	public List<Employee> getAllEmployee(){
		return service.getAllEmployee();
	}

	@PostMapping ( value = "/add")
	public Employee addEmployee(@RequestBody Employee employee) {
		System.out.println("add called" + employee.getEmpNo());
		return service.saveEmployee(employee);
	}
	
	@PutMapping ( value = "/update")
	public Employee updateEmployee(@RequestBody Employee employee) {
		System.out.println("Update called" + employee.getEmpNo());
		return service.saveEmployee(employee);
	}
	
	@DeleteMapping ( value = "/delete/{id}")
	public void deleteEmployee(@PathVariable Integer id) {
		service.deleteEmployee(id);
	}
}
